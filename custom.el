(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(LaTeX-command "latex -synctex=1")
 '(TeX-source-correlate-mode t)
 '(TeX-view-program-list
   (quote
    (("Okular"
      ("okular --unique \\\"file:\\\"%s.pdf\\\"#src:%n %a\\\"")
      ""))))
 '(company-quickhelp-color-background "#4e4e4e")
 '(company-quickhelp-color-foreground "#5fafd7")
 '(company-quickhelp-delay 0.1)
 '(company-quickhelp-mode t)
 '(company-quickhelp-use-propertized-text t)
 '(custom-safe-themes
   (quote
    ("06f0b439b62164c6f8f84fdda32b62fb50b6d00e8b01c2208e55543a6337433a" "628278136f88aa1a151bb2d6c8a86bf2b7631fbea5f0f76cba2a0079cd910f7d" "1b8d67b43ff1723960eb5e0cba512a2c7a2ad544ddb2533a90101fd1852b426e" default)))
 '(elpy-modules
   (quote
    (elpy-module-company elpy-module-eldoc elpy-module-flymake elpy-module-pyvenv elpy-module-yasnippet elpy-module-sane-defaults)))
 '(minimap-hide-fringes t)
 '(minimap-mode t)
 '(minimap-window-location (quote right))
 '(org-agenda-files
   (quote
    ("/home1/kastnerm/Seafile/org/emacs.org" "/home1/kastnerm/Seafile/org/gm.org" "/home1/kastnerm/Seafile/org/inbox.org" "/home1/kastnerm/Seafile/org/phd-diary.org" "/home1/kastnerm/Seafile/org/test.org")))
 '(package-selected-packages
   (quote
    (yasnippet powerline langtool minimap company-quickhelp json-mode pkgbuild-mode git-gutter mozc htmlize company go-mode helm helm-core jedi-core markdown-mode projectile python-environment pythonic swiper pcache org helm-projectile use-package org-ehtml org-preview-html framemove deft company-statistics hl-todo highlight-indentation highlight-chars xpm wanderlust undo-tree swiper-helm spaceline rtags robe pyenv-mode-auto popwin paradox org-projectile moe-theme migemo magit jade-mode helm-ag haml-mode go-eldoc glsl-mode flycheck elpy dracula-theme desktop+ darkokai-theme d-mode company-jedi company-irony company-go color-theme-sanityinc-tomorrow cmake-mode cmake-ide clang-format auctex)))
 '(paradox-github-token t)
 '(safe-local-variable-values (quote ((TeX-engine . platex)))))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(company-scrollbar-bg ((t (:background "#ffffff"))))
 '(company-scrollbar-fg ((t (:background "#ffffff"))))
 '(company-tooltip ((t (:inherit default :background "#ffffff"))))
 '(company-tooltip-common ((t (:inherit font-lock-constant-face))))
 '(company-tooltip-selection ((t (:inherit font-lock-function-name-face))))
 '(minimap-active-region-background ((t (:background "saddle brown"))))
 '(minimap-font-face ((t (:height 20 :family "DejaVu Sans Mono")))))
